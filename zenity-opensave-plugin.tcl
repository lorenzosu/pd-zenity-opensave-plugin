# META NAME Gtk Open/Save dialogs using zenity (http://freecode.com/projects/zenity)
# META DESCRIPTION Gives you a gtk-looking open using zenity command
# META AUTHOR <Lorenzo Sutton> lorenzofsutton@gmail.com
# Contributions and ideas by Hans and yvan
package require Tcl 8.5
package require Tk
package require pdwindow 0.1
package require pd_menus 0.1
package require pd_menucommands 0.1

# Thanks fo Hans for helping getting these right.
rename ::pd_menucommands::menu_open ::pd_menucommands::menu_open_old
rename pdtk_canvas_saveas pdtk_canvas_saveas_old

# Thanks to yvan for the idea.
proc ::zenity_open_save_init {} {
	# We require that zenity exists in the PATH
	set ::zenity_exists 0
	foreach p_dir [split $::env(PATH) ":"] {
			if {[file exists $p_dir/zenity]} {
					set ::zenity_exists 1
					pdtk_post "zenity-opensave-plugin loaded. Have nice, usable open/save experience\n"
					#pdtk_post "EXISTS\n"
			}
	}
	if {! $::zenity_exists} {
		pdtk_post "\nWARNING: zenity-opensave-plugin cannot find zenity command in your PATH. Is it installed?\nWill use Tk open and save dialogs.\n"
	}
}

proc ::zenity_open {the_dir} {
	set zenity_switches { [list
		   	{--file-selection}
			{--multiple}
			{--title=Open} 
			{--file-filter=Pd patch|*.pd}
			{--file-filter=Max patch|*.max}
			{--file-filter=Max Text|*.mxp}
			]
		}
	#what a weird language wish it was Python :/
	lappend zenity_switches --filename=$the_dir/
	# stderror (e.g. GTK warnings will be printed to terminal if any
	if {[catch {exec -ignorestderr zenity {*}$zenity_switches} results options]} {
			#pdtk_post $results
			#pdtk_post $options
			#pdtk_post "Problem"
			return ""
	} else {
			#pdtk_post $results
			#pdtk_post "HERE"
			return $results
	}
}

proc ::zenity_save {the_file} {
	set zenity_switches { [list
		   	{--file-selection}
			{--title=Save}
			{--save}
			{--confirm-overwrite}
			{--file-filter=Pd patch|*.pd}
			{--file-filter=Max patch|*.max}
			{--file-filter=Max Text|*.mxp}
			{--file-filter=All files|*.*}
			]
		}
	#what a weird language wish it was Python :/
	lappend zenity_switches --filename=$::fileopendir/
	if {[catch {exec -ignorestderr zenity {*}$zenity_switches} results options]} {
			return ""
	} else {
			return $results
	}
}
proc ::pd_menucommands::menu_open {} {
	set files ""
	if { $::zenity_exists } {
			set fileList [::zenity_open $::fileopendir]
			if { $fileList != "" } {
					set files [split $fileList "|"]
			}
			pdtk_post $files
    } else {
        if { ! [file isdirectory $::fileopendir]} {set ::fileopendir $::env(HOME)}
        set files [tk_getOpenFile -defaultextension .pd \
	                       -multiple true \
	                       -filetypes $::filetypes \
	                       -initialdir $::fileopendir]
    }
    if {$files ne ""} {
        foreach filename $files { 
            open_file $filename
        }
        set ::fileopendir [file dirname $filename]
    }
}

proc pdtk_canvas_saveas {name initialfile initialdir destroyflag} {
    if { ! [file isdirectory $initialdir]} {
			set initialdir $::env(HOME)
	}
	if { $::zenity_exists } {
		set filename [::zenity_save $initialdir/$initialfile]
  		if {$filename eq ""} return; # Cancel
    } else {
        # if zenity was not found fall-back to usual tk    
        set filename [tk_getSaveFile -initialfile $initialfile -initialdir $initialdir \
                          -defaultextension .pd -filetypes $::filetypes]
        if {$filename eq ""} return; # they clicked cancel
    }
    set extension [file extension $filename]
    set oldfilename $filename
    set filename [regsub -- "$extension$" $filename [string tolower $extension]]
    if { ! [regexp -- "\.(pd|pat|mxt)$" $filename]} {
        # we need the file extention even on Mac OS X
        set filename $filename.pd
    }
    # test again after downcasing and maybe adding a ".pd" on the end
    if {$filename ne $oldfilename && [file exists $filename]} {
        set answer [tk_messageBox -type okcancel -icon question -default cancel\
                        -message [_ "\"$filename\" already exists. Do you want to replace it?"]]
        if {$answer eq "cancel"} return; # they clicked cancel
    }
    set dirname [file dirname $filename]
    set basename [file tail $filename]
    pdsend "$name savetofile [enquote_path $basename] [enquote_path $dirname] \
    $destroyflag"
    set ::filenewdir $dirname	
}

::zenity_open_save_init; #as suggested by yvan  

rename menu_open menu_open_old
namespace import ::pd_menucommands::menu_open
